package dependencies

object UnitTest {

  const val jupiter_api = "org.junit.jupiter:junit-jupiter-api:${Versions.junit_jupiter}"
  const val jupiter_engine = "org.junit.jupiter:junit-jupiter-engine:${Versions.junit_jupiter}"
  const val mock_web_server = "com.squareup.okhttp3:mockwebserver:${Versions.okHttp}"
  const val junit_test = "androidx.test.ext:junit:${Versions.junit_test}"
  const val ui_test_junit4 = "androidx.compose.ui:ui-test-junit4:${Versions.ui_test_junit4}"

}