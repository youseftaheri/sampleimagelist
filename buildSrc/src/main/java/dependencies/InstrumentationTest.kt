package dependencies

object InstrumentationTest {

  const val compose_ui = "androidx.compose.ui:ui-test-junit4:${Versions.compose}"
  const val compose_ui1 = "androidx.test.ext:junit:1.1.3"
}