package dependencies

object Accompanist {

    const val coil = "io.coil-kt:coil-compose:${Versions.coil}"
}