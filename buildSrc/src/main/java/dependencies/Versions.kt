package dependencies

object Versions {

  const val compileSdk = 33
  const val minSdk = 24
  const val targetSdk = 32
  const val versionCode = 1
  const val versionName = "1.0"
  const val jvmTarget = "1.11"
  const val kotlin = "1.8.21"
  const val gradle = "8.0.0"
  const val HiddenSecretsPlugin = "0.2.1"
  const val hilt = "2.44"
  const val junit5 = "1.9.3.0"
  const val kotlin_compiler_extension = "1.4.7"
  const val core_ktx = "1.8.0"
  const val app_compat = "1.6.1"
  const val material = "1.4.0"
  const val constraint_layout = "2.1.3"
  const val androidx_ui = "1.0.0-alpha07"
  const val multidex = "2.0.1"
  const val compose = "1.5.0-alpha04"
  const val compose_constraint = "1.0.1"
  const val compose_activity = "1.7.1"
  const val nav_component = "2.5.3"
  const val nav_compose = "2.5.3"
  const val hilt_navigation = "1.0.0"
  const val retrofit = "2.9.0"
  const val okHttp = "5.0.0-alpha.11"
  const val logging_interceptor = "5.0.0-alpha.6"
  const val leak_canary = "2.11"
  const val room =  "2.5.1"
  const val junit_jupiter = "5.9.3"
  const val datastore = "1.0.0-alpha06"
  const val coil = "2.4.0"
  const val junit_test = "1.1.5"
  const val ui_test_junit4 = "1.4.3"

}
