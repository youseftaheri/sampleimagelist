package com.yousef.sampleimagelist.cache.model

import com.yousef.sampleimagelist.domain.model.Image
import com.yousef.sampleimagelist.domain.util.DomainMapper

class ImageEntityMapper : DomainMapper<ImageEntity, Image> {

    override fun mapToDomainModel(model: ImageEntity, query: String): Image {
        return Image(
            id = model.id,
            user = model.user,
            tags = model.tags,
            previewURL = model.previewURL,
            webformatURL = model.webformatURL,
            comments = model.comments,
            likes = model.likes,
            downloads = model.downloads,
            searchedWord = model.searchedWord,
        )
    }


    override fun mapFromDomainModel(domainModel: Image, query: String): ImageEntity {
        return ImageEntity(
            id = domainModel.id,
            user = domainModel.user,
            tags = domainModel.tags,
            previewURL = domainModel.previewURL,
            webformatURL = domainModel.webformatURL,
            comments = domainModel.comments,
            likes = domainModel.likes,
            downloads = domainModel.downloads,
            searchedWord = query
        )
    }

    fun fromEntityList(initial: List<ImageEntity>, query: String): List<Image> {
        return initial.map { mapToDomainModel(it, query) }
    }

    fun toEntityList(initial: List<Image>, query: String): List<ImageEntity> {
        return initial.map { mapFromDomainModel(it, query) }
    }
}