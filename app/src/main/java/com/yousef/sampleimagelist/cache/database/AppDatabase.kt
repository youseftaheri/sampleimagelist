package com.yousef.sampleimagelist.cache.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.yousef.sampleimagelist.cache.ImageDao
import com.yousef.sampleimagelist.cache.model.ImageEntity

@Database(entities = [ImageEntity::class ], version = 1)
abstract class AppDatabase: RoomDatabase() {

    abstract fun imageDao(): ImageDao

    companion object{
        val DATABASE_NAME: String = "image_db"
    }


}