package com.yousef.sampleimagelist.cache.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "images")
data class ImageEntity(

    // Value from API
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "id")
    var id: Int,

    // Value from API
    @ColumnInfo(name = "user")
    var user: String,

    // Value from API
    @ColumnInfo(name = "tags")
    var tags: String,

    // Value from API
    @ColumnInfo(name = "previewURL")
    var previewURL: String,

    // Value from API
    @ColumnInfo(name = "webformatURL")
    var webformatURL: String,

    // Value from API
    @ColumnInfo(name = "comments")
    var comments: Int,

    // Value from API
    @ColumnInfo(name = "likes")
    var likes: Int,

    // Value from API
    @ColumnInfo(name = "downloads")
    var downloads: Int,

    // Value from API
    @ColumnInfo(name = "searchedWord")
    var searchedWord: String = "",
)