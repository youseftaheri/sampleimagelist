package com.yousef.sampleimagelist.cache

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.yousef.sampleimagelist.cache.model.ImageEntity
import com.yousef.sampleimagelist.util.IMAGE_PAGINATION_PAGE_SIZE

@Dao
interface ImageDao {

  @Insert(onConflict = OnConflictStrategy.IGNORE)
  suspend fun insertImages(images: List<ImageEntity>): LongArray

  @Query("SELECT * FROM images WHERE id = :id")
  suspend fun getImageById(id: Int): ImageEntity?

  @Query("""
        SELECT * FROM images 
        WHERE tags LIKE '%' || :query || '%'
        OR searchedWord LIKE '%' || :query || '%'
        ORDER BY id ASC LIMIT :pageSize OFFSET ((:page - 1) * :pageSize)
        """)
  suspend fun searchImages(
    query: String,
    page: Int,
    pageSize: Int = IMAGE_PAGINATION_PAGE_SIZE
  ): List<ImageEntity>

  @Query("""
        SELECT * FROM images 
        ORDER BY id ASC LIMIT :pageSize OFFSET ((:page - 1) * :pageSize)
    """)
  suspend fun getAllImages(
    page: Int,
    pageSize: Int = IMAGE_PAGINATION_PAGE_SIZE
  ): List<ImageEntity>

}