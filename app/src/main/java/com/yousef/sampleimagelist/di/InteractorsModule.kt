package com.yousef.sampleimagelist.di

import com.yousef.sampleimagelist.network.ImageService
import com.yousef.sampleimagelist.cache.ImageDao
import com.yousef.sampleimagelist.cache.model.ImageEntityMapper
import com.yousef.sampleimagelist.interactors.image.GetImage
import com.yousef.sampleimagelist.interactors.image_list.RestoreImages
import com.yousef.sampleimagelist.interactors.image_list.SearchImages
import com.yousef.sampleimagelist.network.model.ImageDtoMapper
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped

@Module
@InstallIn(ViewModelComponent::class)
object InteractorsModule {

  @ViewModelScoped
  @Provides
  fun provideSearchImage(
    imageService: ImageService,
    imageDao: ImageDao,
    imageEntityMapper: ImageEntityMapper,
    imageDtoMapper: ImageDtoMapper,
  ): SearchImages {
    return SearchImages(
      imageService = imageService,
      imageDao = imageDao,
      entityMapper = imageEntityMapper,
      dtoMapper = imageDtoMapper,
    )
  }

  @ViewModelScoped
  @Provides
  fun provideRestoreImages(
    imageDao: ImageDao,
    imageEntityMapper: ImageEntityMapper,
  ): RestoreImages {
    return RestoreImages(
      imageDao = imageDao,
      entityMapper = imageEntityMapper,
    )
  }

  @ViewModelScoped
  @Provides
  fun provideGetImage(
    imageDao: ImageDao,
    imageEntityMapper: ImageEntityMapper,
  ): GetImage {
    return GetImage(
      imageDao = imageDao,
      entityMapper = imageEntityMapper,
    )
  }

}











