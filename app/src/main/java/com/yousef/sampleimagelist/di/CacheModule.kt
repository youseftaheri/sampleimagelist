package com.yousef.sampleimagelist.di

import androidx.room.Room
import com.yousef.sampleimagelist.cache.ImageDao
import com.yousef.sampleimagelist.cache.database.AppDatabase
import com.yousef.sampleimagelist.cache.model.ImageEntityMapper
import com.yousef.sampleimagelist.presentation.BaseApplication
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object CacheModule {

  @Singleton
  @Provides
  fun provideDb(app: BaseApplication): AppDatabase {
    return Room
      .databaseBuilder(app, AppDatabase::class.java, AppDatabase.DATABASE_NAME)
      .fallbackToDestructiveMigration()
      .build()
  }

  @Singleton
  @Provides
  fun provideImageDao(db: AppDatabase): ImageDao{
    return db.imageDao()
  }

  @Singleton
  @Provides
  fun provideCacheImageMapper(): ImageEntityMapper{
    return ImageEntityMapper()
  }

}







