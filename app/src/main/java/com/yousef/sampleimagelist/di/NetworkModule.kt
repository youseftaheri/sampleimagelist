package com.yousef.sampleimagelist.di

import com.yousef.sampleimagelist.network.ImageService
import com.yousef.sampleimagelist.network.model.ImageDtoMapper
import com.google.gson.GsonBuilder
import com.yousef.sampleimagelist.Secrets
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @Singleton
    @Provides
    fun provideImageMapper(): ImageDtoMapper {
        return ImageDtoMapper()
    }

    @Singleton
    @Provides
    fun provideImageService(): ImageService {
        return Retrofit.Builder()
            .baseUrl("https://pixabay.com/api/")
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .build()
            .create(ImageService::class.java)
    }

    @Singleton
    @Provides
    @Named("auth_token")
    fun provideAuthToken(): String{
        return Secrets().getapikey("com.yousef.sampleimagelist")
    }

}