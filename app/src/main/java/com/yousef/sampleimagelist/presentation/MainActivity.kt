package com.yousef.sampleimagelist.presentation

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.platform.LocalContext
import androidx.hilt.navigation.HiltViewModelFactory
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.multidex.MultiDex
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.yousef.sampleimagelist.presentation.navigation.Screen
import com.yousef.sampleimagelist.presentation.ui.image.ImageDetailScreen
import com.yousef.sampleimagelist.presentation.ui.image.ImageViewModel
import com.yousef.sampleimagelist.presentation.ui.image_list.ImageListEvent
import com.yousef.sampleimagelist.presentation.ui.image_list.ImageListScreen
import com.yousef.sampleimagelist.presentation.ui.image_list.ImageListViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject
import com.yousef.sampleimagelist.presentation.util.ConnectivityManager as ConnMng

@ExperimentalCoroutinesApi
@ExperimentalMaterialApi
@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var connectivityStatusManager: ConnMng

    override fun onDestroy() {
        super.onDestroy()
        connectivityStatusManager.unregisterConnectionObserver(this)
    }

    @ExperimentalComposeUiApi
    override fun onCreate(savedInstanceState: Bundle?) {
        MultiDex.install(applicationContext)
        super.onCreate(savedInstanceState)
        connectivityStatusManager.registerConnectionObserver(this)

        var listUpdated = false

        setContent {
            val navController = rememberNavController()
            NavHost(navController = navController, startDestination = Screen.ImageList.route) {
                composable(route = Screen.ImageList.route) { navBackStackEntry ->
                    val factory = HiltViewModelFactory(LocalContext.current, navBackStackEntry)
                    val viewModel: ImageListViewModel =
                        viewModel(key = "ImageListViewModel", factory = factory)

                    val networkRequest = NetworkRequest.Builder()
                        .addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
                        .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
                        .addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR)
                        .build()

                    val networkCallback = object : ConnectivityManager.NetworkCallback() {
                        override fun onAvailable(network: Network) {
                            super.onAvailable(network)
                            if (!listUpdated) {
                                viewModel.onTriggerEvent(ImageListEvent.NewSearchEvent)
                                if (connectivityStatusManager.isNetworkAvailable.value)
                                    listUpdated = true
                            }
                        }

                        override fun onLost(network: Network) {
                            super.onLost(network)
                            listUpdated = false
                        }
                    }
                    val connectivityManager =
                        getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                    connectivityManager.requestNetwork(networkRequest, networkCallback)

                    ImageListScreen(
                        isNetworkAvailable = connectivityStatusManager.isNetworkAvailable.value,
                        onNavigateToImageDetailScreen = navController::navigate,
                        viewModel = viewModel,
                    )
                }
                composable(
                    route = Screen.ImageDetail.route + "/{imageId}",
                    arguments = listOf(navArgument("imageId") {
                        type = NavType.IntType
                    })
                ) { navBackStackEntry ->
                    val factory = HiltViewModelFactory(LocalContext.current, navBackStackEntry)
                    val viewModel: ImageViewModel =
                        viewModel(key = "ImageDetailViewModel", factory = factory)

                    ImageDetailScreen(
                        isNetworkAvailable = connectivityStatusManager.isNetworkAvailable.value,
                        imageId = navBackStackEntry.arguments?.getInt("imageId"),
                        viewModel = viewModel,
                    )
                }
            }

        }
    }

}