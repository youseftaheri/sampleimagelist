package com.yousef.sampleimagelist.presentation.navigation

sealed class Screen(
    val route: String,
){
    object ImageList: Screen("imageList")
    object ImageDetail: Screen("imageDetail")
}