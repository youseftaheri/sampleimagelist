package com.yousef.sampleimagelist.presentation.ui.image_list

import android.util.Log
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Scaffold
import androidx.compose.material.rememberScaffoldState
import androidx.compose.runtime.Composable
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.platform.LocalContext
import com.yousef.sampleimagelist.presentation.components.ImageList
import com.yousef.sampleimagelist.presentation.components.SearchAppBar
import com.yousef.sampleimagelist.presentation.theme.AppTheme
import com.yousef.sampleimagelist.util.TAG
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalComposeUiApi
@ExperimentalCoroutinesApi
@ExperimentalMaterialApi
@Composable
fun ImageListScreen(
    isNetworkAvailable: Boolean,
    onNavigateToImageDetailScreen: (String) -> Unit,
    viewModel: ImageListViewModel,
) {
    Log.d(TAG, "ImageListScreen: $viewModel")

    val images = viewModel.images.value

    val query = viewModel.query.value

    val loading = viewModel.loading.value

    val page = viewModel.page.value

    val dialogQueue = viewModel.dialogQueue

    val scaffoldState = rememberScaffoldState()

    AppTheme(
        displayProgressBar = loading,
        scaffoldState = scaffoldState,
        isNetworkAvailable = isNetworkAvailable,
        dialogQueue = dialogQueue.queue.value,
    ) {

        Scaffold(
            topBar = {
                SearchAppBar(
                    query = query,
                    onQueryChanged = viewModel::onQueryChanged,
                    onExecuteSearch = {
                        viewModel.onTriggerEvent(ImageListEvent.NewSearchEvent)
                    }
                )
            },
            scaffoldState = scaffoldState,
            snackbarHost = {
                scaffoldState.snackbarHostState
            },
            content = {it
                ImageList(
                    loading = loading,
                    images = images,
                    onChangeScrollPosition = viewModel::onChangeImageScrollPosition,
                    page = page,
                    onTriggerNextPage = { viewModel.onTriggerEvent(ImageListEvent.NextPageEvent) },
                    onNavigateToImageDetailScreen = onNavigateToImageDetailScreen,
                    context = LocalContext.current
                )
            }
        )
    }
}