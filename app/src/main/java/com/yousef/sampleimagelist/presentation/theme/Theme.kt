package com.yousef.sampleimagelist.presentation.theme

import android.annotation.SuppressLint
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import com.yousef.sampleimagelist.presentation.components.*
import java.util.*

@SuppressLint("ConflictingOnColor")
private val ThemeColors = lightColors(
  primary = Blue600,
  primaryVariant = Blue400,
  onPrimary = Black,
  secondary = Color.White,
  secondaryVariant = Teal300,
  onSecondary = Color.Black,
  error = RedErrorDark,
  onError = RedErrorLight,
  background = Grey1,
  onBackground = Color.Black,
  surface = Color.White,
  onSurface = Black,
)

@ExperimentalComposeUiApi
@ExperimentalMaterialApi
@Composable
fun AppTheme(
  isNetworkAvailable: Boolean,
  displayProgressBar: Boolean,
  scaffoldState: ScaffoldState,
  dialogQueue: Queue<GenericDialogInfo>? = null,
  content: @Composable () -> Unit,
) {
  MaterialTheme(
    colors = ThemeColors,
    typography = QuickSandTypography,
    shapes = AppShapes
  ){
    Box(
      modifier = Modifier
        .fillMaxSize()
        .background(color = Grey1)
    ){
      Column{
        ConnectivityMonitor(isNetworkAvailable = isNetworkAvailable)
        content()
      }
      CircularIndeterminateProgressBar(isDisplayed = displayProgressBar, 0.3f)
      DefaultSnackbar(
        snackbarHostState = scaffoldState.snackbarHostState,
        onDismiss = {
          scaffoldState.snackbarHostState.currentSnackbarData?.dismiss()
        },
        modifier = Modifier.align(Alignment.BottomCenter)
      )
      ProcessDialogQueue(
        dialogQueue = dialogQueue,
      )
    }
  }
}

@Composable
fun ProcessDialogQueue(
  dialogQueue: Queue<GenericDialogInfo>?,
) {
  dialogQueue?.peek()?.let { dialogInfo ->
    GenericDialog(
      onDismiss = dialogInfo.onDismiss,
      title = dialogInfo.title,
      description = dialogInfo.description,
      positiveAction = dialogInfo.positiveAction,
      negativeAction = dialogInfo.negativeAction
    )
  }
}








































