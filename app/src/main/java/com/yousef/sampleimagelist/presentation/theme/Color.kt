package com.yousef.sampleimagelist.presentation.theme

import androidx.compose.ui.graphics.Color

val Blue400 = Color(0xFF11212E)
val Blue600 = Color(0xFFFF1100)

val Teal300 = Color(0xFF1AC6FF)

val Grey1 = Color(0xFFF2F2F2)

val Black = Color(0xFF000000)

val RedErrorDark = Color(0xFFB00020)
val RedErrorLight = Color(0xFFEF5350)






