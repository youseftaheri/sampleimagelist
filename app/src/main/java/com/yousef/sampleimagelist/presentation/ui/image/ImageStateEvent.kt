package com.yousef.sampleimagelist.presentation.ui.image

sealed class ImageEvent{

    data class GetImageEvent(
        val id: Int
    ): ImageEvent()

}