package com.yousef.sampleimagelist.presentation.util

import android.app.Application
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.LifecycleOwner
import com.yousef.sampleimagelist.interactors.app.DoesNetworkHaveInternet
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ConnectivityManager
@Inject
constructor(
  application: Application,
) {
  private val connectionLiveData = ConnectionLiveData(application,)

  val isNetworkAvailable = mutableStateOf(DoesNetworkHaveInternet.execute(application))

  fun registerConnectionObserver(lifecycleOwner: LifecycleOwner){
    connectionLiveData.observe(lifecycleOwner) { isConnected ->
      isConnected?.let { isNetworkAvailable.value = it }
    }
  }

  fun unregisterConnectionObserver(lifecycleOwner: LifecycleOwner){
    connectionLiveData.removeObservers(lifecycleOwner)
  }
}