package com.yousef.sampleimagelist.presentation.components

import android.content.Context
import android.content.DialogInterface
import androidx.appcompat.app.AlertDialog
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.itemsIndexed
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.yousef.sampleimagelist.domain.model.Image
import com.yousef.sampleimagelist.presentation.navigation.Screen
import com.yousef.sampleimagelist.presentation.ui.image_list.PAGE_SIZE
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalMaterialApi
@ExperimentalCoroutinesApi
@Composable
fun ImageList(
    loading: Boolean,
    images: List<Image>,
    onChangeScrollPosition: (Int) -> Unit,
    page: Int,
    onTriggerNextPage: () -> Unit,
    onNavigateToImageDetailScreen: (String) -> Unit,
    context: Context,
    ) {
    Box(
        modifier = Modifier
            .background(color = MaterialTheme.colors.surface)
    ) {
        if (loading && images.isEmpty()) {
            LoadingImageListShimmer(imageHeight = 250.dp)
        } else if (images.isEmpty()) {
            NothingHere()
        } else {
            LazyVerticalGrid(
                columns = GridCells.Fixed(2),
                verticalArrangement = Arrangement.spacedBy(4.dp),
                horizontalArrangement = Arrangement.spacedBy(8.dp),
                contentPadding = PaddingValues(start = 8.dp, end = 8.dp)
            ) {
                itemsIndexed(
                    items = images
                ) { index, image ->
                    onChangeScrollPosition(index)
                    if ((index + 1) >= (page * PAGE_SIZE) && !loading) {
                        onTriggerNextPage()
                    }
                    ImageCard(
                        image = image
                    ) {
                        val alert = AlertDialog.Builder(context)
                        alert.setMessage("Do you want to see more details?")
                        alert.setNegativeButton(
                            "No"
                        ) { dialog: DialogInterface, _: Int -> dialog.dismiss() }
                        alert.setPositiveButton("Yes") {
                                _: DialogInterface, _: Int ->
                            val route = Screen.ImageDetail.route + "/${image.id}"
                            onNavigateToImageDetailScreen(route)
                        }
                        alert.setCancelable(true)
                        alert.show()
                    }
                }
            }
        }
    }
}







