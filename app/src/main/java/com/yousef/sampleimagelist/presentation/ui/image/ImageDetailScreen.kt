package com.yousef.sampleimagelist.presentation.ui.image

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Scaffold
import androidx.compose.material.rememberScaffoldState
import androidx.compose.runtime.Composable
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.yousef.sampleimagelist.presentation.components.IMAGE_HEIGHT
import com.yousef.sampleimagelist.presentation.components.LoadingImageShimmer
import com.yousef.sampleimagelist.presentation.components.ImageView
import com.yousef.sampleimagelist.presentation.theme.AppTheme
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalComposeUiApi
@ExperimentalMaterialApi
@ExperimentalCoroutinesApi
@Composable
fun ImageDetailScreen(
    isNetworkAvailable: Boolean,
    imageId: Int?,
    viewModel: ImageViewModel,
) {
    if (imageId == null) {
        TODO("Show Invalid Image")
    } else {
        val onLoad = viewModel.onLoad.value
        if (!onLoad) {
            viewModel.onLoad.value = true
            viewModel.onTriggerEvent(ImageEvent.GetImageEvent(imageId))
        }

        val loading = viewModel.loading.value

        val image = viewModel.image.value

        val dialogQueue = viewModel.dialogQueue

        val scaffoldState = rememberScaffoldState()

        AppTheme(
            displayProgressBar = loading,
            scaffoldState = scaffoldState,
            isNetworkAvailable = isNetworkAvailable,
            dialogQueue = dialogQueue.queue.value,
        ) {
            Scaffold(
                scaffoldState = scaffoldState,
                snackbarHost = {
                    scaffoldState.snackbarHostState
                },
                content = { it
                Box(
                    modifier = Modifier.fillMaxSize()
                ) {
                    if (loading && image == null) {
                        LoadingImageShimmer(imageHeight = IMAGE_HEIGHT.dp)
                    } else if (!loading && image == null && onLoad) {
                        TODO("Show Invalid Image")
                    } else {
                        image?.let { ImageView(image = it) }
                    }
                }
            }
            )
        }
    }
}