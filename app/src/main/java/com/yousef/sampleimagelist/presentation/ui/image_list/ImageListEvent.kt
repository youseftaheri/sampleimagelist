package com.yousef.sampleimagelist.presentation.ui.image_list

sealed class ImageListEvent {
    object NewSearchEvent : ImageListEvent()
    object NextPageEvent : ImageListEvent()
}