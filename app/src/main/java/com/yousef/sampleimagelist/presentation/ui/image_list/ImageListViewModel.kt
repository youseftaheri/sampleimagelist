package com.yousef.sampleimagelist.presentation.ui.image_list

import android.util.Log
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.yousef.sampleimagelist.domain.model.Image
import com.yousef.sampleimagelist.interactors.image_list.RestoreImages
import com.yousef.sampleimagelist.interactors.image_list.SearchImages
import com.yousef.sampleimagelist.presentation.ui.util.DialogQueue
import com.yousef.sampleimagelist.presentation.util.ConnectivityManager
import com.yousef.sampleimagelist.util.TAG
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject
import javax.inject.Named

const val PAGE_SIZE = 30

const val STATE_KEY_PAGE = "image.state.page.key"
const val STATE_KEY_QUERY = "image.state.query.key"
const val STATE_KEY_LIST_POSITION = "image.state.query.list_position"

@HiltViewModel
class ImageListViewModel
@Inject
constructor(
    private val searchImages: SearchImages,
    private val restoreImages: RestoreImages,
    private @Named("auth_token") val token: String,
    private val savedStateHandle: SavedStateHandle,
    private val connectivityManager: ConnectivityManager,
) : ViewModel() {

    val images: MutableState<List<Image>> = mutableStateOf(ArrayList())

    val query = mutableStateOf("fruits")

    val loading = mutableStateOf(false)

    val page = mutableStateOf(1)

    private var imageListScrollPosition = 0

    val dialogQueue = DialogQueue()

    init {
        savedStateHandle.get<Int>(STATE_KEY_PAGE)?.let { p ->
            setPage(p)
        }
        savedStateHandle.get<String>(STATE_KEY_QUERY)?.let { q ->
            setQuery(q)
        }
        savedStateHandle.get<Int>(STATE_KEY_LIST_POSITION)?.let { p ->
            setListScrollPosition(p)
        }

        onTriggerEvent(ImageListEvent.NewSearchEvent)

    }

    fun onTriggerEvent(event: ImageListEvent) {
        viewModelScope.launch {
            try {
                when (event) {
                    is ImageListEvent.NewSearchEvent -> {
                        newSearch()
                    }

                    is ImageListEvent.NextPageEvent -> {
                        nextPage()
                    }
                }
            } catch (e: Exception) {
                Log.e(TAG, "launchJob: Exception: ${e}, ${e.cause}")
                e.printStackTrace()
            } finally {
                Log.d(TAG, "launchJob: finally called.")
            }
        }
    }

    private fun newSearch() {
        Log.d(TAG, "newSearch: query: ${query.value}, page: ${page.value}")
        // New search. Reset the state
        resetSearchState()

        if (connectivityManager.isNetworkAvailable.value) {
            searchImages.execute(
                token = token,
                page = page.value,
                query = query.value,
            )
                .onEach { dataState ->
                    loading.value = dataState.loading

                    dataState.data?.let { list ->
                        images.value = list
                    }

                    dataState.error?.let { error ->
                        Log.e(TAG, "newSearch: $error")
                        dialogQueue.appendErrorMessage("An Error Occurred", error)
                    }
                }.launchIn(viewModelScope)
        } else {
            restoreImages.execute(
                page = page.value,
                query = query.value,
            )
                .onEach { dataState ->
                    loading.value = dataState.loading

                    dataState.data?.let { list ->
                        images.value = list
                    }

                    dataState.error?.let { error ->
                        Log.e(TAG, "restore: $error")
                        dialogQueue.appendErrorMessage("An Error Occurred", error)
                    }
                }.launchIn(viewModelScope)
        }
    }

    private fun nextPage() {
        if ((imageListScrollPosition + 1) >= (page.value * PAGE_SIZE)) {
            incrementPage()
            Log.d(TAG, "nextPage: triggered: ${page.value}")

            if (page.value > 1) {
                if (connectivityManager.isNetworkAvailable.value) {
                    searchImages.execute(
                        token = token,
                        page = page.value,
                        query = query.value
                    )
                        .onEach { dataState ->
                            loading.value = dataState.loading

                            dataState.data?.let { list ->
                                appendImages(list)
                            }

                            dataState.error?.let { error ->
                                Log.e(TAG, "nextPage: $error")
                                dialogQueue.appendErrorMessage("An Error Occurred", error)
                            }
                        }.launchIn(viewModelScope)
                } else {
                    restoreImages.execute(
                        page = page.value,
                        query = query.value
                    )
                        .onEach { dataState ->
                            loading.value = dataState.loading

                            dataState.data?.let { list ->
                                appendImages(list)
                            }

                            dataState.error?.let { error ->
                                Log.e(TAG, "nextPage: $error")
                                dialogQueue.appendErrorMessage("An Error Occurred", error)
                            }
                        }.launchIn(viewModelScope)
                }
            }
        }
    }

    private fun appendImages(images: List<Image>) {
        val current = ArrayList(this.images.value)
        current.addAll(images)
        this.images.value = current
    }

    private fun incrementPage() {
        setPage(page.value + 1)
    }

    fun onChangeImageScrollPosition(position: Int) {
        setListScrollPosition(position = position)
    }

    /**
     * Called when a new search is executed.
     */
    private fun resetSearchState() {
        images.value = listOf()
        page.value = 1
        onChangeImageScrollPosition(0)
    }

    fun onQueryChanged(query: String) {
        setQuery(query)
    }

    private fun setListScrollPosition(position: Int) {
        imageListScrollPosition = position
        savedStateHandle[STATE_KEY_LIST_POSITION] = position
    }

    private fun setPage(page: Int) {
        this.page.value = page
        savedStateHandle[STATE_KEY_PAGE] = page
    }

    private fun setQuery(query: String) {
        this.query.value = query
        savedStateHandle[STATE_KEY_QUERY] = query
    }
}
