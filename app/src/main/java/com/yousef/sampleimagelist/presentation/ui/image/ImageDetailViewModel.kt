package com.yousef.sampleimagelist.presentation.ui.image

import android.util.Log
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.yousef.sampleimagelist.domain.model.Image
import com.yousef.sampleimagelist.interactors.image.GetImage
import com.yousef.sampleimagelist.presentation.ui.util.DialogQueue
import com.yousef.sampleimagelist.util.TAG
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

const val STATE_KEY_IMAGE = "image.state.image.key"

@ExperimentalCoroutinesApi
@HiltViewModel
class ImageViewModel
@Inject
constructor(
    private val getImage: GetImage,
    private val state: SavedStateHandle,
): ViewModel(){

    val image: MutableState<Image?> = mutableStateOf(null)

    val loading = mutableStateOf(false)

    val onLoad: MutableState<Boolean> = mutableStateOf(false)

    val dialogQueue = DialogQueue()

    init {
        state.get<Int>(STATE_KEY_IMAGE)?.let{ imageId ->
            onTriggerEvent(ImageEvent.GetImageEvent(imageId))
        }
    }

    fun onTriggerEvent(event: ImageEvent){
        viewModelScope.launch {
            try {
                when(event){
                    is ImageEvent.GetImageEvent -> {
                        if(image.value == null){
                            getImage(event.id)
                        }
                    }
                }
            }catch (e: Exception){
                Log.e(TAG, "launchJob: Exception: ${e}, ${e.cause}")
                e.printStackTrace()
            }
        }
    }

    private fun getImage(id: Int){
        getImage.execute(id).onEach { dataState ->
            loading.value = dataState.loading

            dataState.data?.let { data ->
                image.value = data
                state[STATE_KEY_IMAGE] = data.id
            }

            dataState.error?.let { error ->
                Log.e(TAG, "getImage: $error")
                dialogQueue.appendErrorMessage("An Error Occurred", error)
            }
        }.launchIn(viewModelScope)
    }
}












