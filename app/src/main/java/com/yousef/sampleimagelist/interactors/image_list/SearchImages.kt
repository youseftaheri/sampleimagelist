package com.yousef.sampleimagelist.interactors.image_list

import com.yousef.sampleimagelist.network.ImageService
import com.yousef.sampleimagelist.cache.ImageDao
import com.yousef.sampleimagelist.cache.model.ImageEntityMapper
import com.yousef.sampleimagelist.domain.data.DataState
import com.yousef.sampleimagelist.domain.model.Image
import com.yousef.sampleimagelist.network.model.ImageDtoMapper
import com.yousef.sampleimagelist.util.IMAGE_PAGINATION_PAGE_SIZE
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class SearchImages(
    private val imageDao: ImageDao,
    private val imageService: ImageService,
    private val entityMapper: ImageEntityMapper,
    private val dtoMapper: ImageDtoMapper,
) {

    fun execute(
        token: String,
        page: Int,
        query: String,
    ): Flow<DataState<List<Image>>> = flow {
        try {
            emit(DataState.loading())

                val images = getImagesFromNetwork(
                    query = query,
                    page = page,
                    perPage = IMAGE_PAGINATION_PAGE_SIZE,
                    token = token,
                )

                // insert into cache
                imageDao.insertImages(entityMapper.toEntityList(images, query))
                emit(DataState.success(images))

        } catch (e: Exception) {
            emit(DataState.error(e.message ?: "Unknown Error"))
        }
    }

    private suspend fun getImagesFromNetwork(
        query: String,
        page: Int,
        perPage: Int,
        token: String
    ): List<Image> {
        return dtoMapper.toDomainList(
            imageService.search(
                query = query,
                page = page,
                perPage = perPage,
                token = token,
            ).images,
            query
        )
    }
}