package com.yousef.sampleimagelist.interactors.image

import com.yousef.sampleimagelist.cache.ImageDao
import com.yousef.sampleimagelist.cache.model.ImageEntityMapper
import com.yousef.sampleimagelist.domain.data.DataState
import com.yousef.sampleimagelist.domain.model.Image
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

/**
 * Retrieve image from the cache given it's unique id.
 */
class GetImage(
    private val imageDao: ImageDao,
    private val entityMapper: ImageEntityMapper,
) {

    fun execute(
        imageId: Int,
    ): Flow<DataState<Image>> = flow {
        try {
            emit(DataState.loading())
            // just to show loading ;)
            delay(1000)
            val image = getImageFromCache(imageId = imageId)
            if (image != null) {
                emit(DataState.success(image))
            } else {
                throw Exception("Unable to get image from the cache.")
            }
        } catch (e: Exception) {
            emit(DataState.error<Image>(e.message ?: "Unknown Error"))
        }
    }

    private suspend fun getImageFromCache(imageId: Int): Image? {
        return imageDao.getImageById(imageId)?.let { imageEntity ->
            entityMapper.mapToDomainModel(imageEntity, imageEntity.searchedWord)
        }
    }
}