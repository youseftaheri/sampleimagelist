package com.yousef.sampleimagelist.interactors.image_list

import com.yousef.sampleimagelist.cache.ImageDao
import com.yousef.sampleimagelist.cache.model.ImageEntityMapper
import com.yousef.sampleimagelist.domain.data.DataState
import com.yousef.sampleimagelist.domain.model.Image
import com.yousef.sampleimagelist.util.IMAGE_PAGINATION_PAGE_SIZE
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

/**
 * Restore a list of images from cache
 */
class RestoreImages(
  private val imageDao: ImageDao,
  private val entityMapper: ImageEntityMapper,
) {
  fun execute(
    page: Int,
    query: String
  ): Flow<DataState<List<Image>>> = flow {
    try {
      emit(DataState.loading())

      val cacheResult = if (query.isBlank()) {
        imageDao.getAllImages(
          page = page,
          pageSize = IMAGE_PAGINATION_PAGE_SIZE
        )
      } else {
        imageDao.searchImages(
          query = query,
          page = page,
          pageSize = IMAGE_PAGINATION_PAGE_SIZE
        )
      }
      val list = entityMapper.fromEntityList(cacheResult, query)
      emit(DataState.success(list))
    }catch (e: Exception){
      emit(DataState.error(e.message?: "Unknown Error"))
    }
  }
}





