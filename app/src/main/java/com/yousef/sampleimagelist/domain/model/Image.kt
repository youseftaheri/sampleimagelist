package com.yousef.sampleimagelist.domain.model

data class Image (
    val id: Int,
    val user: String,
    val tags: String,
    val previewURL: String,
    val webformatURL: String,
    val comments: Int,
    val likes: Int,
    val downloads: Int,
    val searchedWord: String,
)