package com.yousef.sampleimagelist.domain.util

interface DomainMapper <T, DomainModel>{

    fun mapToDomainModel(model: T, query: String): DomainModel

    fun mapFromDomainModel(domainModel: DomainModel, query: String): T
}