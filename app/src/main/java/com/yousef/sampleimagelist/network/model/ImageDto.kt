package com.yousef.sampleimagelist.network.model

import com.google.gson.annotations.SerializedName

data class ImageDto(

        @SerializedName("id")
        var id: Int,

        @SerializedName("user")
        var user: String,

        @SerializedName("tags")
        var tags: String,

        @SerializedName("previewURL")
        var previewURL: String,

        @SerializedName("webformatURL")
        var webformatURL: String,

        @SerializedName("comments")
        var comments: Int,

        @SerializedName("likes")
        var likes: Int,

        @SerializedName("downloads")
        var downloads: Int,
)












