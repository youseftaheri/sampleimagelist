package com.yousef.sampleimagelist.network.response

import com.yousef.sampleimagelist.network.model.ImageDto
import com.google.gson.annotations.SerializedName

data class ImageSearchResponse(

    @SerializedName("totalHits")
        var count: Int,

    @SerializedName("hits")
        var images: List<ImageDto>,
)