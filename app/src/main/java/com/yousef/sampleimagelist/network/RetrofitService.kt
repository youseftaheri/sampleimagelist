package com.yousef.sampleimagelist.network

import com.yousef.sampleimagelist.network.response.ImageSearchResponse
import retrofit2.http.GET
import retrofit2.http.Query


interface ImageService {
    companion object {
        private const val IMAGE_TYPE = "photo"
        private const val COMMON_PARAMS = "image_type=$IMAGE_TYPE"
    }

    @GET("?$COMMON_PARAMS")
    suspend fun search(
        @Query("q") query: String,
        @Query("page") page: Int,
        @Query("per_page") perPage: Int = 30,
        @Query("key") token: String
    ): ImageSearchResponse
}











