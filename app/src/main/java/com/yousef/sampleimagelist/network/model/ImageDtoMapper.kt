package com.yousef.sampleimagelist.network.model

import com.yousef.sampleimagelist.domain.model.Image
import com.yousef.sampleimagelist.domain.util.DomainMapper

class ImageDtoMapper : DomainMapper<ImageDto, Image> {

    override fun mapToDomainModel(model: ImageDto, query: String): Image {
        return Image(
            id = model.id,
            user = model.user,
            tags = model.tags,
            previewURL = model.previewURL,
            webformatURL = model.webformatURL,
            comments = model.comments,
            likes = model.likes,
            downloads = model.downloads,
            searchedWord = query,
        )
    }

    override fun mapFromDomainModel(domainModel: Image, query: String): ImageDto {
        return ImageDto(
            id = domainModel.id,
            user = domainModel.user,
            tags = domainModel.tags,
            previewURL = domainModel.previewURL,
            webformatURL = domainModel.webformatURL,
            comments = domainModel.comments,
            likes = domainModel.likes,
            downloads = domainModel.downloads,
        )
    }

    fun toDomainList(initial: List<ImageDto>, query: String): List<Image>{
        return initial.map { mapToDomainModel(it, query) }
    }

}
