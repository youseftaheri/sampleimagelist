package com.yousef.sampleimagelist.cache

import com.yousef.sampleimagelist.cache.model.ImageEntity

class ImageDaoFake(
    private val appDatabaseFake: AppDatabaseFake
): ImageDao {

    override suspend fun insertImages(images: List<ImageEntity>): LongArray {
        appDatabaseFake.images.addAll(images)
        return longArrayOf(1) // return success
    }

    override suspend fun getImageById(id: Int): ImageEntity? {
        return appDatabaseFake.images.find { it.id == id }
    }

    override suspend fun searchImages(
        query: String,
        page: Int,
        pageSize: Int
    ): List<ImageEntity> {
        return appDatabaseFake.images
    }

    override suspend fun getAllImages(page: Int, pageSize: Int): List<ImageEntity> {
        return appDatabaseFake.images
    }

}
