package com.yousef.sampleimagelist.cache

import com.yousef.sampleimagelist.cache.model.ImageEntity

class AppDatabaseFake {
    val images = mutableListOf<ImageEntity>()
}
