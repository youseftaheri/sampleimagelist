package com.yousef.sampleimagelist.factory

import com.yousef.sampleimagelist.cache.model.ImageEntity

class PhotoFactory {
    companion object {
        fun createPhotos(count: Int): List<ImageEntity> {
            return (1..count).map {
                ImageEntity(
                    id = it,
                    user = "user:$it",
                    previewURL = "previewURL:$it",
                    tags = "tags:$it",
                    webformatURL = "webformatURL:$it",
                    comments = it,
                    likes = it,
                    downloads = it,
                    searchedWord = "searchedWord:$it"
                )
            }
        }
    }

}