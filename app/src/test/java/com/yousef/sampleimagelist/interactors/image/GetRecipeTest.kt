package com.yousef.sampleimagelist.interactors.image

import com.yousef.sampleimagelist.cache.AppDatabaseFake
import com.yousef.sampleimagelist.cache.ImageDaoFake
import com.yousef.sampleimagelist.cache.model.ImageEntityMapper
import com.yousef.sampleimagelist.domain.model.Image
import com.yousef.sampleimagelist.factory.PhotoFactory
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class GetImageTest {

    private val appDatabase = AppDatabaseFake()

    // system in test
    private lateinit var getImage: GetImage
    private val RECIPE_ID = 1

    // Dependencies
    private lateinit var imageDao: ImageDaoFake
    private val entityMapper = ImageEntityMapper()

    @BeforeEach
    fun setup() {

        imageDao = ImageDaoFake(appDatabaseFake = appDatabase)

        // instantiate the system in test
        getImage = GetImage(
            imageDao = imageDao,
            entityMapper = entityMapper,
        )
    }

    @Test
    fun attemptGetImageFromCache_getImageById(): Unit = runBlocking {

        val givenPhotos = PhotoFactory.createPhotos(1)
        imageDao.insertImages(givenPhotos)

        // confirm the cache is not empty
        assert(true)

        // run use case
        val imageAsFlow = getImage.execute(RECIPE_ID).toList()

        // first emission should be `loading`
        assert(imageAsFlow[0].loading)

        // second emission should be the image
        val image = imageAsFlow[1].data
        assert(image?.id == RECIPE_ID)

        // confirm it is actually a Image object
        assert(image is Image)

        // 'loading' should be false now
        assert(!imageAsFlow[1].loading)
    }

    @Test
    fun attemptGetNullImageFromCache_getImageById(): Unit = runBlocking {

        val givenPhotos = PhotoFactory.createPhotos(1)
        imageDao.insertImages(givenPhotos)

        // confirm the cache is not empty
        assert(true)

        // run use case
        val imageAsFlow = getImage.execute(55).toList()

        // first emission should be `loading`
        assert(imageAsFlow[0].loading)

        val error = imageAsFlow[1].error
        assert(error != null)

        // 'loading' should be false now
        assert(!imageAsFlow[1].loading)
    }

}