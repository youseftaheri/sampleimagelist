package com.yousef.sampleimagelist.interactors.image_list

import com.yousef.sampleimagelist.cache.AppDatabaseFake
import com.yousef.sampleimagelist.cache.ImageDaoFake
import com.yousef.sampleimagelist.cache.model.ImageEntityMapper
import com.yousef.sampleimagelist.domain.model.Image
import com.yousef.sampleimagelist.factory.PhotoFactory
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test


class RestoreImagesTest {

    private val appDatabase = AppDatabaseFake()
    private val DUMMY_QUERY = "fruits green"

    private lateinit var restoreImages: RestoreImages

    // Dependencies
    private lateinit var imageDao: ImageDaoFake
    private val entityMapper = ImageEntityMapper()

    @BeforeEach
    fun setup() {
        imageDao = ImageDaoFake(appDatabaseFake = appDatabase)

        // instantiate the system in test
        restoreImages = RestoreImages(
            imageDao = imageDao,
            entityMapper = entityMapper,
        )
    }

    @Test
    fun getImagesFromCache_WithEmptyQuery_andEmitImages(): Unit = runBlocking {

        // confirm the cache is empty to start
        assert(imageDao.getAllImages(1, 30).isEmpty())

        val givenPhotos = PhotoFactory.createPhotos(30)
        imageDao.insertImages(givenPhotos)

        val flowItems = restoreImages.execute(1, "").toList()

        // confirm the cache is no longer empty
        assert(imageDao.getAllImages(1, 30).isNotEmpty())

        // first emission should be `loading`
        assert(flowItems[0].loading)

        // Second emission should be the list of images
        val images = flowItems[1].data
        assert((images?.size ?: 0) > 0)

        // confirm they are actually Image objects
        assert(images?.get(index = 0) is Image)

        assert(!flowItems[1].loading) // loading should be false now
    }

    @Test
    fun getImagesFromCache_WithQuery_andEmitImages(): Unit = runBlocking {

        // confirm the cache is empty to start
        assert(imageDao.searchImages(DUMMY_QUERY, 1, 30).isEmpty())

        val givenPhotos = PhotoFactory.createPhotos(30)
        imageDao.insertImages(givenPhotos)

        val flowItems = restoreImages.execute(1, "").toList()

        // confirm the cache is no longer empty
        assert(imageDao.searchImages(DUMMY_QUERY, 1, 30).isNotEmpty())

        // first emission should be `loading`
        assert(flowItems[0].loading)

        // Second emission should be the list of images
        val images = flowItems[1].data
        assert((images?.size ?: 0) > 0)

        // confirm they are actually Image objects
        assert(images?.get(index = 0) is Image)

        assert(!flowItems[1].loading) // loading should be false now
    }

}