package com.yousef.sampleimagelist.interactors.image_list

import com.google.gson.GsonBuilder
import com.yousef.sampleimagelist.cache.AppDatabaseFake
import com.yousef.sampleimagelist.cache.ImageDaoFake
import com.yousef.sampleimagelist.cache.model.ImageEntityMapper
import com.yousef.sampleimagelist.domain.model.Image
import com.yousef.sampleimagelist.network.ImageService
import com.yousef.sampleimagelist.network.data.MockWebServerResponses.imageListResponse
import com.yousef.sampleimagelist.network.model.ImageDtoMapper
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.runBlocking
import okhttp3.HttpUrl
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.net.HttpURLConnection


class SearchImagesTest {

    private val appDatabase = AppDatabaseFake()
    private lateinit var mockWebServer: MockWebServer
    private lateinit var baseUrl: HttpUrl
    private val DUMMY_TOKEN = "hv8765jhgv7656"
    private val DUMMY_QUERY = "fruits green"

    // system in test
    private lateinit var searchImages: SearchImages

    // Dependencies
    private lateinit var imageService: ImageService
    private lateinit var imageDao: ImageDaoFake
    private val dtoMapper = ImageDtoMapper()
    private val entityMapper = ImageEntityMapper()

    @BeforeEach
    fun setup() {
        mockWebServer = MockWebServer()
        mockWebServer.start()
        baseUrl = mockWebServer.url("/api/")
        imageService = Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .build()
            .create(ImageService::class.java)

        imageDao = ImageDaoFake(appDatabaseFake = appDatabase)

        // instantiate the system in test
        searchImages = SearchImages(
            imageDao = imageDao,
            imageService = imageService,
            entityMapper = entityMapper,
            dtoMapper = dtoMapper,
        )
    }

    @Test
    fun getImagesFromNetwork_andEmitImages(): Unit = runBlocking {

        // condition the response
        mockWebServer.enqueue(
            MockResponse()
                .setResponseCode(HttpURLConnection.HTTP_OK)
                .setBody(imageListResponse)
        )

        // confirm the cache is empty to start
        assert(imageDao.getAllImages(1, 30).isEmpty())

        val flowItems = searchImages.execute(DUMMY_TOKEN, 1, DUMMY_QUERY).toList()

        // confirm the cache is no longer empty
        assert(imageDao.getAllImages(1, 30).isNotEmpty())

        // first emission should be `loading`
        assert(flowItems[0].loading)

        // Second emission should be the list of images
        val images = flowItems[1].data
        assert((images?.size ?: 0) > 0)

        // confirm they are actually Image objects
        assert(images?.get(index = 0) is Image)

        assert(!flowItems[1].loading) // loading should be false now
    }

    @Test
    fun getImagesFromNetwork_emitHttpError(): Unit = runBlocking {

        // condition the response
        mockWebServer.enqueue(
            MockResponse()
                .setResponseCode(HttpURLConnection.HTTP_BAD_REQUEST)
                .setBody("{}")
        )

        val flowItems = searchImages.execute(
            DUMMY_TOKEN,
            1,
            DUMMY_QUERY
        ).toList()

        // first emission should be `loading`
        assert(flowItems[0].loading)

        // Second emission should be the exception
        val error = flowItems[1].error
        assert(error != null)

        assert(!flowItems[1].loading) // loading should be false now
    }

    @AfterEach
    fun tearDown() {
        mockWebServer.shutdown()
    }
}